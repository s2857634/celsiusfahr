package com.example.fahrenheitcelsius;

public class Convert {
    double getConversion(Double celsius) {
        double fahrenheit = 0;
        if (celsius != null) {
            fahrenheit = (celsius*1.8) + 32;
        }
        return fahrenheit;
    }
}
